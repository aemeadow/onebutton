﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class NoteHitter : MonoBehaviour
{
    public delegate void BeatAction();
    public static event BeatAction OnBeatHit, OnBeatMissed;

    AudioSource feedbackSpeaker;

    public AudioClip hit, miss;

    // Start is called before the first frame update
    void Start()
    {
        feedbackSpeaker = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            if (Physics.CheckBox(transform.position, Vector3.one, transform.rotation, LayerMask.GetMask("Beats")))
            {
                if (OnBeatHit != null)
                    OnBeatHit();

                feedbackSpeaker.PlayOneShot(hit);
            }
            else
            {
                if (OnBeatMissed != null)
                    OnBeatMissed();


                feedbackSpeaker.PlayOneShot(miss);
            }
        }

    }
}
