﻿using UnityEngine;
using System.Collections;

public class moveroad : MonoBehaviour
{

    public float speed = 1;
    
    void Update()
    {
        // Move the object forward along its z axis 1 unit/second.
        transform.Translate(new Vector3(speed,0,0) * Time.deltaTime);

       
    }
}