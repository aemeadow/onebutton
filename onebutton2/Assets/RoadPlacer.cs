﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadPlacer : MonoBehaviour
{
    public GameObject parentRoad;
    public GameObject roadSection;
    public float spacing;
    public float yoffset = -5.549769f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time % 2.0f == 0) MakeObject(0.5f);
    }

    void MakeObject(float timeFromPlayer)
    {
        // Make a new object
        GameObject newObj = Instantiate(roadSection) as GameObject;
        newObj.transform.SetParent(parentRoad.transform);
        // Place the new object a specific distance from the player so it takes "timeFromPlayer" seconds to reach the object
        newObj.transform.position = (transform.position + Vector3.forward * spacing * timeFromPlayer) + new Vector3(0, yoffset, 0);
    }
}
