﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using TMPro.Examples;


public class OutrunFontManager : MonoBehaviour
{
    //Basically this is gonna double the text and put the blue one in front of the neon pink one, so it has a depth effect
    //This script just keeps that straight

    public Color secondaryColor;
    public float x_offset, y_offset;
    public TextMeshProUGUI combometer;
    string currentText;
    bool beenAtThisCombo;
    [HideInInspector]
    public static int combo { get; private set; }
    GameObject child;
    TextMeshProUGUI childText;
    public TextMeshProUGUI comboCommentText;
    VertexJitter jitters, childJitters;
    float missFadeDuration = 1f;
    float missFadeRemaining = 0;
    
    void Start()
    {
        jitters = combometer.GetComponent<VertexJitter>();
        child = Instantiate<GameObject>(combometer.gameObject);
        child.transform.SetParent(combometer.gameObject.transform);
        child.transform.localPosition = new Vector3(x_offset, y_offset, -1);
        currentText = combometer.text;
        childText = child.GetComponent<TextMeshProUGUI>();
        childText.color = secondaryColor;
        childJitters = child.GetComponent<VertexJitter>();

        combo = 0;

    }

    private void OnEnable()
    {
        NoteHitter.OnBeatHit += NoteHitter_OnBeatHit;
        NoteHitter.OnBeatMissed += NoteHitter_OnBeatMissed;
    }

    private void OnDisable()
    {
        NoteHitter.OnBeatHit -= NoteHitter_OnBeatHit;
        NoteHitter.OnBeatMissed -= NoteHitter_OnBeatMissed;
    }

    private void NoteHitter_OnBeatHit()
    {
        combo++;
    }

    private void NoteHitter_OnBeatMissed()
    {
        combo = 0;
        comboCommentText.text = "MISS";
        comboCommentText.color = Color.red;
        comboCommentText.CrossFadeAlpha(1, 0, true);
        comboCommentText.CrossFadeAlpha(0, missFadeDuration, false);
    }

    // Update is called once per frame
    void Update()
    {


        combometer.text = combo + "x";

        

        currentText = combometer.text;
        if (!currentText.Equals(childText.text)) childText.text = currentText;
        
        if (combo % 10 == 0)
        {
            int multi = combo / 10;
            if(multi <= 3)
            {
                jitters.AngleMultiplier = multi;
                childJitters.AngleMultiplier = multi;
            }
            jitters.SpeedMultiplier = multi;
            childJitters.SpeedMultiplier = multi;

            
        }

        if(combo % 5 == 0 && combo != 0 && !beenAtThisCombo)
        {
            int multi = combo / 5;
            beenAtThisCombo = true;
            comboCommentText.CrossFadeAlpha(1, 0, true);
            switch (multi)
            {
                case 1:
                    comboCommentText.text = "DECENT COMBO";
                    break;

                case 2:
                    comboCommentText.text = "SLICK COMBO";
                    break;

                case 3:
                    comboCommentText.text = "RADICAL COMBO";
                    break;

                case 4:
                    comboCommentText.text = "GNARLY COMBO";
                    break;

                case 5:
                    comboCommentText.text = "INCREDIBLE COMBO";
                    break;

                case 6:
                    comboCommentText.text = "FEARLESS COMBO";
                    break;

                case 7:
                    comboCommentText.text = "INSANE COMBO";
                    break;

                case 8:
                    comboCommentText.text = "LIGHTSPEED COMBO";
                    break;

                case 9:
                    comboCommentText.text = "BREAKING THE COMBO BARRIER";
                    break;

                case 10:
                    comboCommentText.text = "SPACETIME-ALTERING COMBO";
                    break;

                case 11:
                    comboCommentText.text = "COMBOMETER OVERLOAD";
                    break;

                default:
                    comboCommentText.text = "COMBOMETER OVERLOAD";
                    break;
            }

            comboCommentText.color = Random.ColorHSV(0.5f+(0.01f*multi), 1, 1, 1, 1, 1, 1, 1);
            
        }
        else if(combo % 5 != 0)
        {
            beenAtThisCombo = false;
        }

    }
}
