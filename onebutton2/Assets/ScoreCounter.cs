﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreCounter : MonoBehaviour
{

    [HideInInspector]
    public int score { get; private set; }

    TextMeshProUGUI scoreCounter;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        scoreCounter = GetComponent<TextMeshProUGUI>();
    }

    private void OnEnable()
    {
        NoteHitter.OnBeatHit += NoteHitter_OnBeatHit;
    }

    private void NoteHitter_OnBeatHit()
    {
        score += 100 * (OutrunFontManager.combo == 0 ? 1 : OutrunFontManager.combo);
        scoreCounter.text = "SCORE: " + score;
    }

    private void OnDisable()
    {
        NoteHitter.OnBeatHit -= NoteHitter_OnBeatHit;
    }

}
